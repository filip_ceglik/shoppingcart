﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class ProductUserControl : UserControl
    {
        public ProductUserControl()
        {
            InitializeComponent();
        }

        public void Update(Product product)
        {
            labelName.Text = product.Name;
            label1.Text = product.Description;
            pictureBox1.Load(product.Url);
        }
    }
}