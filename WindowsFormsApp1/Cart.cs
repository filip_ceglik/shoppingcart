﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace WindowsFormsApp1
{

    public class Cart
    {
        public List<Product> cartItems = new List<Product>();

        public void AddItem(Product product)
        {
            cartItems.Add(product);
        }

        public decimal ApplyDiscount()
        {
            var generalDiscount = 0m;
            var bakingMatDiscount = 0m;
            var bodyCandleDiscount = 0m;
            if (cartItems.Count(x => x.Name == "Silicone Baking Mat") > 1)
            {
                generalDiscount += cartItems.Where(x => x.Name == "Silicone Baking Mat").Sum(c => c.Price) - (cartItems.Where(x => x.Name == "Silicone Baking Mat").Sum(c => c.Price) * 0.9m);
            }
            
            if (cartItems.Count(x => x.Name == "Mind & Body Candle") > 2)
            {
                generalDiscount += cartItems.Where(x => x.Name == "Mind & Body Candle").Sum(c => c.Price) - (cartItems.Where(x => x.Name == "Mind & Body Candle").Sum(c => c.Price) * 0.85m);
            }

            if (cartItems.Select(x => x.Category).Distinct().Count() > 1)
            {
                generalDiscount += cartItems.Sum(x => x.Price) - (cartItems.Sum(x => x.Price) * 0.9m);
            }

            
            var sum = cartItems.Sum(x => x.Price) - generalDiscount;
            return sum;
        }

        public decimal SumItems()
        {
            return cartItems.Sum(x => x.Price);
        }
    }

}