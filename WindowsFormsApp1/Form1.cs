﻿using System;
using System.Globalization;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        private readonly ProductService _productService = new ProductService();
        private Product _product;
        private Cart _cart = new Cart();
        private CultureInfo culture = CultureInfo.CreateSpecificCulture("en-US");
        public Form1()
        {
            InitializeComponent();
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            //throw new System.NotImplementedException();
        }

        private void button1_Click(object sender, EventArgs e)
        {
           productUserControl1.Update(_productService.Products[0]);
           _product = _productService.Products[0];
        }

        private void button2_Click(object sender, EventArgs e)
        {
            productUserControl1.Update(_productService.Products[1]);
            _product = _productService.Products[1];
        }

        private void button3_Click(object sender, EventArgs e)
        {
            productUserControl1.Update(_productService.Products[2]);
            _product = _productService.Products[2];
        }

        private void button4_Click(object sender, EventArgs e)
        {
            productUserControl1.Update(_productService.Products[3]);
            _product = _productService.Products[3];
        }

        private void button5_Click(object sender, EventArgs e)
        {
            productUserControl1.Update(_productService.Products[4]);
            _product = _productService.Products[4];
        }

        private void button6_Click(object sender, EventArgs e)
        {
            productUserControl1.Update(_productService.Products[5]);
            _product = _productService.Products[5];
        }

        private void button7_Click_1(object sender, EventArgs e)
        {
            _cart.AddItem(_product);
            label3.Text = _cart.SumItems().ToString("C",culture);
            label7.Text = _cart.ApplyDiscount().ToString("C",culture);
            label5.Text = (_cart.SumItems() - _cart.ApplyDiscount()).ToString("C",culture);
        }
    }
}